package hu.raven.puppet.games.ui.opengl

import android.content.Context
import android.opengl.GLSurfaceView
import hu.raven.puppet.games.R.raw.per_pixel_fragment_shader
import hu.raven.puppet.games.R.raw.per_pixel_vertex_shader
import hu.raven.puppet.games.ui.opengl.layer.ADrawableLayer
import hu.raven.puppet.games.utility.math.FloatMatrix4x4
import hu.raven.puppet.games.utility.math.VectorUtil.vectorOf
import hu.raven.puppet.games.utility.opengl.facade.OpenGlFacade
import hu.raven.puppet.games.utility.opengl.facade.OpenGlFacade.disableOpenGlSetting
import hu.raven.puppet.games.utility.opengl.facade.OpenGlFacade.enableOpenGlSetting
import hu.raven.puppet.games.utility.opengl.facade.OpenGlFacade.setBackgroundClearColor
import hu.raven.puppet.games.utility.opengl.facade.OpenGlFacade.setOpenGlBlendFunctions
import hu.raven.puppet.games.utility.opengl.facade.OpenGlFacade.setOpenGlViewPort
import hu.raven.puppet.games.utility.opengl.facade.enumeration.OpenGlBlendFunction.ONE_MINUS_SOURCE_ALPHA
import hu.raven.puppet.games.utility.opengl.facade.enumeration.OpenGlBlendFunction.SOURCE_ALPHA
import hu.raven.puppet.games.utility.opengl.facade.enumeration.OpenGlSetting.*
import hu.raven.puppet.games.utility.opengl.facade.handle.OpenGlProgramHandle
import hu.raven.puppet.games.utility.opengl.facade.handle.OpenGlUniformVariableHandle
import hu.raven.puppet.games.utility.opengl.util.OpenGlUtil.use
import hu.raven.puppet.opengltutorial.opengl.util.OpenGlShaderUtil.compileOpenGlShaders
import javax.microedition.khronos.opengles.GL10

class Renderer(val context: Context) : GLSurfaceView.Renderer {

    val floatPerTextureCoord: Int = 2
    val floatPerPositionCoord: Int = 2
    val bytesPerFloat: Int = 4

    /** Store the view matrix. This can be thought of as our camera. This matrix transforms world space to eye space;
     * it positions things relative to our eye.     */
    val mViewMatrix = FloatMatrix4x4()
    val mProjectionMatrix = FloatMatrix4x4()

    /** This will be used to pass in the texture.  */
    var mTextureUniformHandle: OpenGlUniformVariableHandle? = null


    /** This is a handle to our cube shading program.  */
    var mProgramHandle: OpenGlProgramHandle? = null


    var layers: List<ADrawableLayer> = listOf()

    private val vertexShader: String by lazy {
        context.resources.openRawResource(per_pixel_vertex_shader).bufferedReader().readText()
    }
    private val fragmentShader: String by lazy {
        context.resources.openRawResource(per_pixel_fragment_shader).bufferedReader().readText()
    }

    override fun onSurfaceCreated(
        glUnused: GL10?,
        config: javax.microedition.khronos.egl.EGLConfig?
    ) {
        setBackgroundClearColor(vectorOf(1.0f, 1.0f, 1.0f, 1.0f))

        disableOpenGlSetting(REMOVE_BACK_FACE_BY_CULLING)
        disableOpenGlSetting(USE_DEPTH_TEST)
        enableOpenGlSetting(BLEND_COLORS)
        setOpenGlBlendFunctions(SOURCE_ALPHA, ONE_MINUS_SOURCE_ALPHA)

        mProgramHandle = compileOpenGlShaders(
            vertexShader,
            fragmentShader,
            arrayOf("a_Position", "a_TexCoordinate")
        )

    }

    override fun onSurfaceChanged(glUnused: GL10, width: Int, height: Int) {
        // Set the OpenGL viewport to the same size as the surface.
        setOpenGlViewPort(
            vectorOf(0, 0),
            vectorOf(width, height)
        )

        // Create a new perspective projection matrix. The height will stay the same
        // while the width will vary as per aspect ratio.
        val ratio = width.toFloat() / height
        val left = -ratio
        val bottom = -1.0f
        val top = 1.0f
        val nearDistance = 1.0f
        val farDistance = 10.0f
        mProjectionMatrix.setToFrustum(
            vectorOf(left, ratio),
            vectorOf(bottom, top),
            vectorOf(nearDistance, farDistance)
        )

        println("width: $width")
        println("height: $height")

        val positionOfCamera = vectorOf(2.0f / height * width, 2.0f, 1.0f)
        val lookAtPositionOfCamera = vectorOf(2.0f / height * width, 2.0f, 0.0f)
        val upDirectionOfCamera = vectorOf(0f, 1f, 0f)

        // Set the view matrix. This matrix can be said to represent the camera position.
        // NOTE: In OpenGL 1, a ModelView matrix is used, which is a combination of a model and
        // view matrix. In OpenGL 2, we can keep track of these matrices separately if we choose.
        mViewMatrix.setToLookAt(
            positionOfCamera,
            lookAtPositionOfCamera,
            upDirectionOfCamera
        )
    }

    override fun onDrawFrame(glUnused: GL10) {
        OpenGlFacade.clearScreen()

        mProgramHandle?.use {
            mTextureUniformHandle = getUniformHandle("u_Texture")
            layers.forEach { it.draw() }
        }

    }

    /**Initialize the model data.    */
    fun initialize() {
        layers.forEach { it.initialize() }
    }

    fun update() {
    }
}