package hu.raven.puppet.games.ui.opengl.layer

import hu.raven.puppet.games.logic.game_object.IRenderable
import hu.raven.puppet.games.ui.opengl.Renderer

class GridFreeLayer(
    override val renderer: Renderer,
    override val textureResourceId: Int
) : ADrawableLayer() {

    override val rectangleCount: Int
        get() = dataArray.size
    override lateinit var positionArray: FloatArray
    override lateinit var textureCoordinateArray: FloatArray
    lateinit var dataArray: Array<IRenderable>

    override fun loadVisualBoxesToPositionArray() {
        positionArray = FloatArray(rectangleCount * renderer.floatPerPositionCoord * 6)
        dataArray.forEachIndexed { renderableIndex, renderableData ->
            rotateArray(
                renderableData.visualBox.serialize(),
                renderableData.visualBox.rightDirection.toIntVector()
            ).forEachIndexed { index, trapFloat ->
                positionArray[renderableIndex * 2 * 6 + index] = trapFloat
            }
        }
    }

    override fun loadTextureBoxesToTextureArray() {
        textureCoordinateArray = FloatArray(rectangleCount * 6 * renderer.floatPerTextureCoord)
        //The points texture position initialise
        dataArray.forEachIndexed { characterIndex, characterData ->
            rotateArray(
                characterData.textureBox.serialize(),
                characterData.textureBox.rightDirection.toIntVector()
            ).forEachIndexed { index, trapFloat ->
                textureCoordinateArray[characterIndex * 2 * 6 + index] = trapFloat
            }
        }
    }


    override fun initialize() {
        loadVisualBoxesToPositionArray()

        loadTextureBoxesToTextureArray()

        // Initialize feature the buffers.
        loadFloatArraysToBuffers()
    }
}