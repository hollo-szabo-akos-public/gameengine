package hu.raven.puppet.games.utility.opengl.facade.handle

import android.opengl.GLES20
import hu.raven.puppet.games.utility.math.FloatMatrix4x4
import hu.raven.puppet.games.utility.math.FloatVector4D
import hu.raven.puppet.games.utility.math.toFloatArray
import hu.raven.puppet.games.utility.opengl.facade.enumeration.OpenGlTargetTextureType


@JvmInline
value class OpenGlUniformVariableHandle(val uniformHandle: Int) {

    fun bindUniformMatrix(
        matrixArray: FloatMatrix4x4,
        doTranspose: Boolean = false,
        matrixCount: Int = 1,
        offset: Int = 0,
    ) {
        GLES20.glUniformMatrix4fv(
            uniformHandle,
            matrixCount,
            doTranspose,
            matrixArray.content,
            offset
        )
    }

    fun bindUniformVector(
        vector: FloatVector4D,
        vectorCount: Int = 1,
        offset: Int = 0,
    ) {
        GLES20.glUniform4fv(uniformHandle, vectorCount, vector.toFloatArray(), offset)
    }

    fun addUniformTextureToTextureSampler(samplerIndex: Int) {
        // Tell the texture uniform sampler to use this texture in the shader by binding to texture unit 0.
        GLES20.glUniform1i(uniformHandle, samplerIndex)
    }

    fun bindUniformTexture(targetTextureType: OpenGlTargetTextureType) {
        GLES20.glBindTexture(targetTextureType.code, uniformHandle)
    }
}