package hu.raven.puppet.games.ui

import android.content.Context
import dagger.Module
import dagger.Provides
import hu.raven.puppet.games.ui.game.MainPresenter
import javax.inject.Singleton

@Module
class UIModule(private val context: Context) {

    fun context() = context

    @Provides
    @Singleton
    fun gamePresenter() = MainPresenter()

}