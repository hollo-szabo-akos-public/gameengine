package hu.raven.puppet.games.utility.opengl.facade.enumeration

import android.opengl.GLES20

enum class OpenGlPrametriParam(val code: Int) {
    TEXTURE_MIN_FILTER(GLES20.GL_TEXTURE_MIN_FILTER),
    TEXTURE_MAG_FILTER(GLES20.GL_TEXTURE_MAG_FILTER),
}