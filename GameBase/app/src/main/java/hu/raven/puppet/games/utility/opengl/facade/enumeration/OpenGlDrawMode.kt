package hu.raven.puppet.games.utility.opengl.facade.enumeration

import android.opengl.GLES20

enum class OpenGlDrawMode(val code: Int) {
    DRAW_TRIANGLES(GLES20.GL_TRIANGLES)
}