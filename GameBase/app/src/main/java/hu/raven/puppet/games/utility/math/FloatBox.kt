package hu.raven.puppet.games.utility.math

data class FloatBox(
    val centerPosition: FloatVector2D,
    val size: FloatVector2D,
    val rotation: Float,
)
