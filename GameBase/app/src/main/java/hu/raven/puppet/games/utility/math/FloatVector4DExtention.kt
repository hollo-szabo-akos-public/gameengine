package hu.raven.puppet.games.utility.math


fun FloatVector4D.toFloatArray() = floatArrayOf(x, y, z, w)