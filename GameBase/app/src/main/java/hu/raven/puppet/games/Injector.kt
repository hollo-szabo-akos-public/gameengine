package hu.raven.puppet.games

import android.app.Activity

val Activity.injector: GameBaseApplicationComponent
    get() {
        return (this.applicationContext as GameBaseApplication).injector
    }