package hu.raven.puppet.games.logic.game_object

import hu.raven.puppet.games.utility.math.vector_extension.FloatVector

interface IDynamic : IInteractable {
    var acceleration: FloatVector
    var speed: FloatVector
    var weight: Float
    fun addForce(force: FloatVector) {
        acceleration += force / weight
    }

    fun stop() {
        acceleration.vector.indices.forEach { acceleration[it] = 0.0f }
        speed.vector.indices.forEach { speed[it] = 0.0f }
    }

    fun onMove(tickCount: Int)
}