package hu.raven.puppet.games.utility.opengl.facade.handle

import android.opengl.GLES20

@JvmInline
value class OpenGlShaderHandle(val shaderHandle: Int) {

    fun deleteShader() {
        GLES20.glDeleteShader(shaderHandle)
    }


    fun getShaderInfoLog(): String {
        return GLES20.glGetShaderInfoLog(shaderHandle)
    }

    fun getCompileStatus(): IntArray {
        val result = IntArray(1)
        GLES20.glGetShaderiv(shaderHandle, GLES20.GL_COMPILE_STATUS, result, 0)
        return result
    }

    fun didCreationFail(): Boolean {
        return shaderHandle == 0
    }

    fun loadSourceCode(sourceCode: String) {
        GLES20.glShaderSource(shaderHandle, sourceCode)
    }

    fun compile() {
        GLES20.glCompileShader(shaderHandle)
    }
}