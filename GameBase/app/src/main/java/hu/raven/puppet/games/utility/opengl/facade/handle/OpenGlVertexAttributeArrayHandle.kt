package hu.raven.puppet.games.utility.opengl.facade.handle

import android.opengl.GLES20
import hu.raven.puppet.games.utility.opengl.facade.enumeration.OpenGlVertexAttributeArrayType
import java.nio.Buffer

@JvmInline
value class OpenGlVertexAttributeArrayHandle(val vertexAttributeArrayHandle: Int) {

    fun enable() {
        GLES20.glEnableVertexAttribArray(vertexAttributeArrayHandle)
    }

    fun disable() {
        GLES20.glDisableVertexAttribArray(vertexAttributeArrayHandle)
    }


    fun bindVertexAttributeArray(
        vertexAttributeArrayType: OpenGlVertexAttributeArrayType,
        buffer: Buffer,
        coordsPerVertex: Int,
        bytesPerCoord: Int,
        isNormalized: Boolean
    ) {
        // Prepare the triangle coordinate data
        GLES20.glVertexAttribPointer(
            vertexAttributeArrayHandle,
            coordsPerVertex,
            vertexAttributeArrayType.code,
            isNormalized,
            coordsPerVertex * bytesPerCoord,
            buffer
        )
    }
}