package hu.raven.puppet.games.utility

import hu.raven.puppet.games.utility.math.vector_extension.IntVector
import hu.raven.puppet.games.utility.math.vector_extension.IntVector.Companion.toIntVector

enum class EDirection2D {
    LEFT {
        override val vector = intArrayOf(-1, 0).toIntVector()
        override val invers by lazy { RIGHT }
    },
    UP {
        override val vector = intArrayOf(0, 1).toIntVector()
        override val invers by lazy { DOWN }
    },
    RIGHT {
        override val vector = intArrayOf(1, 0).toIntVector()
        override val invers by lazy { LEFT }
    },
    DOWN {
        override val vector = intArrayOf(0, -1).toIntVector()
        override val invers by lazy { UP }
    };

    abstract val vector: IntVector
    abstract val invers: EDirection2D
}