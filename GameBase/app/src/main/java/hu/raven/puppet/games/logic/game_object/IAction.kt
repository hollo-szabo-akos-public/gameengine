package hu.raven.puppet.games.logic.game_object

interface IAction {
    val id: String
    val animation: IAnimation
    fun onTrigger(tickCount: Int)
}