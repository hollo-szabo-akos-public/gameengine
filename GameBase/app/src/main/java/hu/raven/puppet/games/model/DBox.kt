package hu.raven.puppet.games.model

import hu.raven.puppet.games.utility.math.vector_extension.FloatVector

data class DBox(
    val centerPosition: FloatVector,
    val size: FloatVector,
    val rightDirection: FloatVector
)
