package hu.raven.puppet.games.utility.math.matrix_extension

import hu.raven.puppet.games.utility.math.vector_extension.DoubleVector
import hu.raven.puppet.games.utility.math.vector_extension.FloatVector
import hu.raven.puppet.games.utility.math.vector_extension.IntVector
import hu.raven.puppet.games.utility.math.vector_extension.LongVector

@JvmInline
value class LongMatrix(val matrix: Array<LongVector>) {

    constructor(size: Int, initializer: (index: Int) -> LongVector)
            : this(Array(size, initializer))

    constructor(size: IntVector, initializer: (indexes: IntVector) -> Long)
            : this(
        Array(size.x) { columnIndex ->
            LongVector(size.y) { lineIndex ->
                initializer(
                    IntVector(
                        intArrayOf(
                            columnIndex, lineIndex
                        )
                    )
                )
            }
        })

    inline val size
        get() = matrix.size

    operator fun get(indexes: IntVector) = matrix[indexes.y][indexes.x]

    operator fun minus(other: IntMatrix): LongMatrix {
        if (matrix.size != other.size ||
            matrix.mapIndexed { index, line -> line.size != other[index].size }
                .reduce { stuff, value -> stuff || value }
        ) throw IndexOutOfBoundsException("Matrices are not the right size!")

        return LongMatrix(matrix.size) { lineIndex ->
            matrix[lineIndex] - other[lineIndex]
        }
    }

    operator fun minus(other: LongMatrix): LongMatrix {
        if (matrix.size != other.size ||
            matrix.mapIndexed { index, line -> line.size != other[index].size }
                .reduce { stuff, value -> stuff || value }
        ) throw IndexOutOfBoundsException("Matrices are not the right size!")

        return LongMatrix(matrix.size) { lineIndex ->
            matrix[lineIndex] - other[lineIndex]
        }
    }

    operator fun minus(other: FloatMatrix): FloatMatrix {
        if (matrix.size != other.size ||
            matrix.mapIndexed { index, line -> line.size != other[index].size }
                .reduce { stuff, value -> stuff || value }
        ) throw IndexOutOfBoundsException("Matrices are not the right size!")

        return FloatMatrix(matrix.size) { lineIndex ->
            matrix[lineIndex] - other[lineIndex]
        }
    }

    operator fun minus(other: DoubleMatrix) = matrix.run {
        if (size != other.size ||
            this.mapIndexed { index, line -> line.size != other[index].size }
                .reduce { stuff, value -> stuff || value }
        ) throw IndexOutOfBoundsException("Matrices are not the right size!")

        DoubleMatrix(size) { lineIndex ->
            this[lineIndex] - other[lineIndex]
        }
    }

    operator fun plus(other: IntMatrix): LongMatrix {
        if (matrix.size != other.size ||
            matrix.mapIndexed { index, line -> line.size != other[index].size }
                .reduce { stuff, value -> stuff || value }
        ) throw IndexOutOfBoundsException("Matrices are not the right size!")

        return LongMatrix(size) { lineIndex ->
            matrix[lineIndex] + other[lineIndex]
        }
    }

    operator fun plus(other: LongMatrix) {
        if (matrix.size != other.size ||
            matrix.mapIndexed { index, line -> line.size != other[index].size }
                .reduce { stuff, value -> stuff || value }
        ) throw IndexOutOfBoundsException("Matrices are not the right size!")

        LongMatrix(size) { lineIndex ->
            matrix[lineIndex] + other[lineIndex]
        }
    }

    operator fun plus(other: FloatMatrix) {
        if (matrix.size != other.size ||
            matrix.mapIndexed { index, line -> line.size != other[index].size }
                .reduce { stuff, value -> stuff || value }
        ) throw IndexOutOfBoundsException("Matrices are not the right size!")

        FloatMatrix(size) { lineIndex ->
            matrix[lineIndex] + other[lineIndex]
        }
    }

    operator fun plus(other: DoubleMatrix) {
        if (matrix.size != other.size ||
            matrix.mapIndexed { index, line -> line.size != other[index].size }
                .reduce { stuff, value -> stuff || value }
        ) throw IndexOutOfBoundsException("Matrices are not the right size!")

        DoubleMatrix(size) { lineIndex ->
            matrix[lineIndex] + other[lineIndex]
        }
    }

    operator fun times(other: Int) = matrix.run {
        LongMatrix(size) { lineIndex ->
            this[lineIndex] * other
        }
    }

    operator fun times(other: Long) = matrix.run {
        LongMatrix(size) { lineIndex ->
            this[lineIndex] * other
        }
    }

    operator fun times(other: Float) = matrix.run {
        FloatMatrix(size) { lineIndex ->
            this[lineIndex] * other
        }
    }

    operator fun times(other: Double) = matrix.run {
        DoubleMatrix(size) { lineIndex ->
            this[lineIndex] * other
        }
    }

    operator fun times(other: IntMatrix) = matrix.run {
        if (this.any { it.size != other.size })
            throw IndexOutOfBoundsException("Matrices are not the right size!")

        if (this.any { it.size != this[0].size } || other.matrix.any { it.size != other.size })
            throw IndexOutOfBoundsException("Matrices line size is inconsistent!")

        LongMatrix(size) { lineIndex ->
            LongVector(other[0].size) {
                this[lineIndex].dot(IntVector(other.size) { other[it][lineIndex] })
            }
        }
    }

    operator fun times(other: LongMatrix) = matrix.run {
        if (this.any { it.size != other.size })
            throw IndexOutOfBoundsException("Matrices are not the right size!")
        if (this.any { it.size != this[0].size } || other.matrix.any { it.size != other.size })
            throw IndexOutOfBoundsException("Matrices line size is inconsistent!")

        LongMatrix(size) { lineIndex ->
            LongVector(other[0].size) {
                this[lineIndex].dot(LongVector(other.size) { other[it][lineIndex] })
            }
        }
    }

    operator fun times(other: FloatMatrix) = matrix.run {
        if (this.any { it.size != other.size })
            throw IndexOutOfBoundsException("Matrices are not the right size!")
        if (this.any { it.size != this[0].size } || other.matrix.any { it.size != other.size })
            throw IndexOutOfBoundsException("Matrices line size is inconsistent!")

        DoubleMatrix(size) { lineIndex ->
            DoubleVector(other[0].size) {
                this[lineIndex].dot(FloatVector(other.size) { other[it][lineIndex] })
            }
        }
    }

    operator fun times(other: DoubleMatrix) = matrix.run {
        if (this.any { it.size != other.size })
            throw IndexOutOfBoundsException("Matrices are not the right size!")

        if (this.any { it.size != this[0].size } || other.matrix.any { it.size != other.size })
            throw IndexOutOfBoundsException("Matrices line size is inconsistent!")

        DoubleMatrix(size) { lineIndex ->
            DoubleVector(other[0].size) {
                this[lineIndex].dot(DoubleVector(other.size) { other[it][lineIndex] })
            }
        }
    }

    operator fun times(other: IntVector) = matrix.run {
        if (this.any { it.size != other.size })
            throw IndexOutOfBoundsException("Matrices are not the right size!")

        LongVector(size) { index ->
            this[index].dot(other)
        }
    }

    operator fun times(other: LongVector) = matrix.run {
        if (this.any { it.size != other.size })
            throw IndexOutOfBoundsException("Matrices are not the right size!")

        LongVector(size) { index ->
            this[index].dot(other)
        }
    }

    operator fun times(other: FloatVector) = matrix.run {
        if (this.any { it.size != other.size })
            throw IndexOutOfBoundsException("Matrices are not the right size!")

        DoubleVector(size) { index ->
            this[index].dot(other)
        }
    }

    operator fun times(other: DoubleVector) = matrix.run {
        if (this.any { it.size != other.size })
            throw IndexOutOfBoundsException("Matrices are not the right size!")

        DoubleVector(size) { index ->
            this[index] dot other
        }
    }

    operator fun get(index: Int) = matrix[index]
}