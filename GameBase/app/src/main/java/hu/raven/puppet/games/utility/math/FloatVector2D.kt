package hu.raven.puppet.games.utility.math

data class FloatVector2D(
    val x: Float,
    val y: Float
)
