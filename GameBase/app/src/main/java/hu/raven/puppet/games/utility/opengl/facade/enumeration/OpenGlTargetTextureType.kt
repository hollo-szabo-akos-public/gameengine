package hu.raven.puppet.games.utility.opengl.facade.enumeration

import android.opengl.GLES20

enum class OpenGlTargetTextureType(val code: Int) {
    TEXTURE_2D(GLES20.GL_TEXTURE_2D)
}