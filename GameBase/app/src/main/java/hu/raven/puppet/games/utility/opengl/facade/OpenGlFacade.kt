package hu.raven.puppet.games.utility.opengl.facade

import android.opengl.GLES20
import hu.raven.puppet.games.utility.math.FloatVector4D
import hu.raven.puppet.games.utility.math.IntVector2D
import hu.raven.puppet.games.utility.opengl.facade.enumeration.OpenGlBlendFunction
import hu.raven.puppet.games.utility.opengl.facade.enumeration.OpenGlSetting
import hu.raven.puppet.games.utility.opengl.facade.enumeration.OpenGlShaderType
import hu.raven.puppet.games.utility.opengl.facade.handle.OpenGlProgramHandle
import hu.raven.puppet.games.utility.opengl.facade.handle.OpenGlShaderHandle

object OpenGlFacade {
    fun setBackgroundClearColor(color: FloatVector4D) {
        GLES20.glClearColor(color.x, color.y, color.z, color.w)
    }

    fun clearScreen() {
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT)
    }

    fun setOpenGlViewPort(leftDownCornerPosition: IntVector2D, screenSize: IntVector2D) {
        GLES20.glViewport(
            leftDownCornerPosition.x,
            leftDownCornerPosition.y,
            screenSize.x,
            screenSize.y
        )
    }

    fun generateTextures(textureCount: Int, handleArray: IntArray, offset: Int = 0) {
        GLES20.glGenTextures(textureCount, handleArray, offset)
    }

    fun createShaderOfType(type: OpenGlShaderType): OpenGlShaderHandle {
        val shaderHandle = GLES20.glCreateShader(type.code)
        return OpenGlShaderHandle(shaderHandle)
    }

    fun createProgram(): OpenGlProgramHandle {
        val programHandle = GLES20.glCreateProgram()
        return OpenGlProgramHandle(programHandle)
    }

    fun enableOpenGlSetting(setting: OpenGlSetting) {
        GLES20.glEnable(setting.code)
    }

    fun disableOpenGlSetting(setting: OpenGlSetting) {
        GLES20.glDisable(setting.code)
    }

    fun setOpenGlBlendFunctions(sourceFactor: OpenGlBlendFunction, destinationFactor: OpenGlBlendFunction) {
        GLES20.glBlendFunc(sourceFactor.code,destinationFactor.code)
    }

}