package hu.raven.puppet.games.utility.math


val FloatBox.cornerCoordinates
    get() = arrayOf(
        VectorUtil.vectorOf(
            centerPosition.x + size.x / 2,
            centerPosition.y + size.y / 2,
        ),
        VectorUtil.vectorOf(
            centerPosition.x + size.x / 2,
            centerPosition.y - size.y / 2,
        ),
        VectorUtil.vectorOf(
            centerPosition.x - size.x / 2,
            centerPosition.y - size.y / 2,
        ),
        VectorUtil.vectorOf(
            centerPosition.x - size.x / 2,
            centerPosition.y + size.y / 2,
        ),
    )