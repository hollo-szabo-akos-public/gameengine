package hu.raven.puppet.opengltutorial.opengl.util

import android.util.Log
import hu.raven.puppet.games.utility.opengl.facade.OpenGlFacade.createProgram
import hu.raven.puppet.games.utility.opengl.facade.OpenGlFacade.createShaderOfType
import hu.raven.puppet.games.utility.opengl.facade.enumeration.OpenGlShaderType
import hu.raven.puppet.games.utility.opengl.facade.handle.OpenGlProgramHandle
import hu.raven.puppet.games.utility.opengl.facade.handle.OpenGlShaderHandle

//load shader like:
// mainActivity.resources.openRawResource(R.raw.per_pixel_vertex_shader)
// .bufferedReader()
// .readLines()
// .joinToString("\n")
object OpenGlShaderUtil {
    private const val TAG = "ShaderHelper"

    private fun OpenGlShaderHandle.onCompilationFail() {
        Log.e(TAG, "Error compiling shader: " + getShaderInfoLog())
        deleteShader()
        throw RuntimeException("Error creating shader.")
    }

    private fun OpenGlShaderHandle.checkIfCompilationWasSuccessful() {
        // Get the compilation status.
        val compileStatus = getCompileStatus()
        // If the compilation failed, delete the shader.
        if (compileStatus[0] == 0) onCompilationFail()
    }

    private fun OpenGlShaderHandle.checkCreation() {
        if (didCreationFail()) {
            throw RuntimeException("Error creating shader.")
        }
    }

    private fun loadShader(type: OpenGlShaderType, shaderCode: String): OpenGlShaderHandle {
        return createShaderOfType(type)
            .apply {
                checkCreation()
                loadSourceCode(shaderCode)
                compile()
                checkIfCompilationWasSuccessful()
            }
    }

    private fun OpenGlProgramHandle.checkCreation() {
        if (didCreationFail()) {
            throw RuntimeException("Error creating shader.")
        }
    }

    private fun OpenGlProgramHandle.checkIfLinkingWasSuccessful() {

        // Get the link status.
        val linkStatus = getLinkStatus()

        // If the link failed, delete the program.
        if (linkStatus[0] == 0) {
            Log.e(TAG, "Error compiling program: " + getProgramInfoLog())
            deleteProgram()
            throw RuntimeException("Error creating program.")
        }
    }

    fun compileOpenGlShaders(
        vertexShaderCode: String,
        fregmentShaderCode: String,
        attributes: Array<String> = arrayOf()
    ): OpenGlProgramHandle {
        val vertexShader: OpenGlShaderHandle =
            loadShader(OpenGlShaderType.VERTEX_SHADER, vertexShaderCode)
        val fragmentShader: OpenGlShaderHandle =
            loadShader(OpenGlShaderType.FRAGMENT_SHADER, fregmentShaderCode)

        // create empty OpenGL ES Program
        return createProgram().apply {
            checkCreation()

            attachShader(vertexShader)
            attachShader(fragmentShader)

            attributes.forEachIndexed { index, attribute ->
                bindAttributeLocation(index, attribute)
            }

            linkProgram()
            checkIfLinkingWasSuccessful()
        }
    }
}