package hu.raven.puppet.games.utility.opengl.util.extention

import android.opengl.GLES20
import hu.raven.puppet.games.utility.opengl.facade.enumeration.OpenGlDrawMode

fun FloatArray.sizeInBytes() = size * Float.SIZE_BYTES

fun FloatArray.vertexCount(floatPerVertex: Int) = size / floatPerVertex

fun FloatArray.draw(drawMode: OpenGlDrawMode, startIndex: Int, floatPerVertex: Int) {
    GLES20.glDrawArrays(
        drawMode.code,
        startIndex,
        vertexCount(floatPerVertex)
    )
}