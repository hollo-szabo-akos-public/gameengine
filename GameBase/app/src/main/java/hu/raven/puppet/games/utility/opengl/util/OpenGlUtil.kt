package hu.raven.puppet.games.utility.opengl.util

import hu.raven.puppet.games.utility.math.FloatMatrix4x4
import hu.raven.puppet.games.utility.math.FloatVector4D
import hu.raven.puppet.games.utility.opengl.facade.OpenGlFacade
import hu.raven.puppet.games.utility.opengl.facade.enumeration.OpenGlDrawMode
import hu.raven.puppet.games.utility.opengl.facade.enumeration.OpenGlTargetTextureType
import hu.raven.puppet.games.utility.opengl.facade.enumeration.OpenGlVertexAttributeArrayType
import hu.raven.puppet.games.utility.opengl.facade.handle.OpenGlProgramHandle
import hu.raven.puppet.games.utility.opengl.facade.handle.OpenGlUniformVariableHandle
import hu.raven.puppet.games.utility.opengl.facade.handle.OpenGlVertexAttributeArrayHandle
import hu.raven.puppet.games.utility.opengl.util.extention.draw
import java.nio.FloatBuffer

object OpenGlUtil {

    fun OpenGlProgramHandle.use(function: OpenGlProgramHandle.() -> Unit) {
        bind()
        function()
    }

    fun OpenGlProgramHandle.bindUniformFloatVector4D(
        uniformVariableName: String,
        vector: FloatVector4D
    ): OpenGlUniformVariableHandle {
        val vectorHandle = getUniformHandle(uniformVariableName)

        vectorHandle.bindUniformVector(vector)

        return vectorHandle
    }

    fun OpenGlProgramHandle.bindUniformMatrix(
        uniformVariableName: String,
        matrix: FloatMatrix4x4
    ): OpenGlUniformVariableHandle {
        val matrixHandle = getUniformHandle(uniformVariableName)

        matrixHandle.bindUniformMatrix(matrix)

        return matrixHandle
    }

    fun OpenGlProgramHandle.bindUniform2DTexture(
        uniformVariableName: String,
        textureUnitIndex: Int
    ): OpenGlUniformVariableHandle {
        return getUniformHandle(uniformVariableName).apply {
            activateTexture(textureUnitIndex)
            bindUniformTexture(OpenGlTargetTextureType.TEXTURE_2D)
            addUniformTextureToTextureSampler(textureUnitIndex)
        }
    }


    fun OpenGlVertexAttributeArrayHandle.bindFloatVertexAttributeArray(
        coordsPerVertex: Int,
        floatBuffer: FloatBuffer
    ) = OpenGlFacade.run {
        enable()
        floatBuffer.position(0)
        bindVertexAttributeArray(
            OpenGlVertexAttributeArrayType.OUT_OF_FLOAT,
            floatBuffer,
            coordsPerVertex,
            Float.SIZE_BYTES,
            false,
        )
    }

    fun FloatArray.drawTriangle(coordsPerVertex: Int) {
        draw(OpenGlDrawMode.DRAW_TRIANGLES, 0, coordsPerVertex)
    }
}