package hu.raven.puppet.games.utility.opengl.facade.enumeration

import android.opengl.GLES20

enum class OpenGlBlendFunction(val code: Int) {
    SOURCE_ALPHA(GLES20.GL_SRC_ALPHA),
    ONE_MINUS_SOURCE_ALPHA(GLES20.GL_ONE_MINUS_SRC_ALPHA)
}