package hu.raven.puppet.games.utility.opengl.facade.handle

import android.graphics.Bitmap
import android.opengl.GLUtils
import hu.raven.puppet.games.utility.opengl.facade.enumeration.OpenGlTargetTextureType

@JvmInline
value class OpenGlBitmapHandle(val bitmap: Bitmap) {
    fun setAsTexture(
        targetTextureType: OpenGlTargetTextureType,
        detailLevel: Int = 0,
        addedBorder: Int = 0
    ) {
        GLUtils.texImage2D(
            targetTextureType.code,
            detailLevel,
            bitmap,
            addedBorder
        )
    }
}