package hu.raven.puppet.games.utility.opengl.facade.handle

import android.opengl.GLES20

@JvmInline
value class OpenGlProgramHandle(val programHandle: Int) {

    fun getVertexAttributeArrayHandle(vertexAttributeArrayName: String): OpenGlVertexAttributeArrayHandle {
        val vertexAttributeArrayHandle =
            GLES20.glGetAttribLocation(programHandle, vertexAttributeArrayName)
        return OpenGlVertexAttributeArrayHandle(vertexAttributeArrayHandle)
    }

    fun getUniformHandle(variableName: String): OpenGlUniformVariableHandle {
        val uniformHandle = GLES20.glGetUniformLocation(programHandle, variableName)
        return OpenGlUniformVariableHandle(uniformHandle)
    }

    fun attachShader(shaderHandle: OpenGlShaderHandle) {
        GLES20.glAttachShader(programHandle, shaderHandle.shaderHandle)
    }

    fun bindAttributeLocation(index: Int, attribute: String) {
        GLES20.glBindAttribLocation(programHandle, index, attribute)
    }

    fun linkProgram() {
        GLES20.glLinkProgram(programHandle)
    }

    fun getLinkStatus(): IntArray {
        val result = IntArray(1)
        GLES20.glGetShaderiv(programHandle, GLES20.GL_LINK_STATUS, result, 0)
        return result
    }

    fun getProgramInfoLog(): String {
        return GLES20.glGetProgramInfoLog(programHandle)
    }

    fun deleteProgram() {
        return GLES20.glDeleteProgram(programHandle)
    }

    fun didCreationFail(): Boolean {
        return programHandle == 0
    }

    fun bind() {
        GLES20.glUseProgram(programHandle)
    }

    fun activateTexture(textureUnitIndex: Int) {
        if (textureUnitIndex !in 0 until 32)
            throw IndexOutOfBoundsException()

        GLES20.glActiveTexture(GLES20.GL_TEXTURE0 + textureUnitIndex)
    }
}