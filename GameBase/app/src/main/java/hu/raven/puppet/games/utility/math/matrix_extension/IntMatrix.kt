package hu.raven.puppet.games.utility.math.matrix_extension

import hu.raven.puppet.games.utility.math.vector_extension.DoubleVector
import hu.raven.puppet.games.utility.math.vector_extension.FloatVector
import hu.raven.puppet.games.utility.math.vector_extension.IntVector
import hu.raven.puppet.games.utility.math.vector_extension.LongVector

@JvmInline
value class IntMatrix(val matrix: Array<IntVector>) {

    constructor(size: Int, initializer: (index: Int) -> IntVector)
            : this(Array(size, initializer))

    constructor(size: IntVector, initializer: (indexes: IntVector) -> Int)
            : this(
        Array(size.x) { columnIndex ->
            IntVector(size.y) { lineIndex: Int ->
                initializer(
                    IntVector(
                        intArrayOf(
                            columnIndex, lineIndex
                        )
                    )
                )
            }
        })

    inline val size
        get() = matrix.size

    operator fun get(indexes: IntVector) = matrix[indexes.y][indexes.x]
    operator fun get(index: Int) = matrix[index]

    operator fun minus(other: IntMatrix) {
        if (size != other.size ||
            (0 until size).any { index -> this[index].size != other[index].size }
        ) throw IndexOutOfBoundsException("Matrices are not the right size!")

        IntMatrix(size) { lineIndex: Int ->
            matrix[lineIndex] - other[lineIndex]
        }
    }

    operator fun minus(other: LongMatrix) {
        if (size != other.size ||
            (0 until size).any { index -> this[index].size != other[index].size }
        ) throw IndexOutOfBoundsException("Matrices are not the right size!")

        LongMatrix(size) { lineIndex: Int ->
            matrix[lineIndex] - other[lineIndex]
        }
    }

    operator fun minus(other: FloatMatrix) {
        if (size != other.size ||
            (0 until size).any { index -> this[index].size != other[index].size }
        ) throw IndexOutOfBoundsException("Matrices are not the right size!")

        FloatMatrix(size) { lineIndex: Int ->
            matrix[lineIndex] - other[lineIndex]
        }
    }

    operator fun minus(other: DoubleMatrix) {
        if (size != other.size ||
            (0 until size).any { index -> this[index].size != other[index].size }
        ) throw IndexOutOfBoundsException("Matrices are not the right size!")

        DoubleMatrix(size) { lineIndex: Int ->
            matrix[lineIndex] - other[lineIndex]
        }
    }

    operator fun plus(other: IntMatrix) {
        if (size != other.size ||
            (0 until size).any { index -> this[index].size != other[index].size }
        ) throw IndexOutOfBoundsException("Matrices are not the right size!")

        IntMatrix(size) { lineIndex: Int ->
            matrix[lineIndex] + other[lineIndex]
        }
    }

    operator fun plus(other: LongMatrix) {
        if (size != other.size ||
            (0 until size).any { index -> this[index].size != other[index].size }
        ) throw IndexOutOfBoundsException("Matrices are not the right size!")

        LongMatrix(size) { lineIndex: Int ->
            matrix[lineIndex] + other[lineIndex]
        }
    }

    operator fun plus(other: FloatMatrix) {
        if (size != other.size ||
            (0 until size).any { index -> this[index].size != other[index].size }
        ) throw IndexOutOfBoundsException("Matrices are not the right size!")

        FloatMatrix(size) { lineIndex: Int ->
            matrix[lineIndex] + other[lineIndex]
        }
    }

    operator fun plus(other: DoubleMatrix) {
        if (size != other.size ||
            (0 until size).any { index -> this[index].size != other[index].size }
        ) throw IndexOutOfBoundsException("Matrices are not the right size!")

        DoubleMatrix(size) { lineIndex: Int -> matrix[lineIndex] + other[lineIndex] }
    }

    operator fun times(other: Int) = matrix.run {
        IntMatrix(size) { lineIndex: Int -> this[lineIndex] * other }
    }

    operator fun times(other: Long) = matrix.run {
        LongMatrix(size) { lineIndex: Int -> this[lineIndex] * other }
    }

    operator fun times(other: Float) = matrix.run {
        FloatMatrix(size) { lineIndex: Int -> this[lineIndex] * other }
    }

    operator fun times(other: Double) = matrix.run {
        DoubleMatrix(size) { lineIndex: Int -> this[lineIndex] * other }
    }

    operator fun times(other: IntMatrix) = matrix.run {
        if (this.any { it.size != other.size })
            throw IndexOutOfBoundsException("Matrices are not the right size!")

        if (this.any { it.size != this[0].size } || other.matrix.any { it.size != other.size })
            throw IndexOutOfBoundsException("Matrices line size is inconsistent!")

        IntMatrix(size) { lineIndex: Int ->
            IntVector(other[0].size) {
                this[lineIndex] dot IntVector(other.size) { other[it][lineIndex] }
            }
        }
    }

    operator fun times(other: LongMatrix) = matrix.run {
        if (this.any { it.size != other.size })
            throw IndexOutOfBoundsException("Matrices are not the right size!")

        if (this.any { it.size != this[0].size } || other.matrix.any { it.size != other.size })
            throw IndexOutOfBoundsException("Matrices line size is inconsistent!")

        LongMatrix(size) { lineIndex: Int ->
            LongVector(other[0].size) {
                this[lineIndex] dot LongVector(other.size) { other[it][lineIndex] }
            }
        }
    }

    operator fun times(other: FloatMatrix) = matrix.run {
        if (this.any { it.size != other.size })
            throw IndexOutOfBoundsException("Matrices are not the right size!")

        if (this.any { it.size != this[0].size } || other.matrix.any { it.size != other.size })
            throw IndexOutOfBoundsException("Matrices line size is inconsistent!")

        DoubleMatrix(size) { lineIndex: Int ->
            DoubleVector(other[0].size) {
                this[lineIndex] dot FloatVector(other.size) { other[it][lineIndex] }
            }
        }
    }

    operator fun times(other: DoubleMatrix) = matrix.run {
        if (this.any { it.size != other.size })
            throw IndexOutOfBoundsException("Matrices are not the right size!")

        if (this.any { it.size != this[0].size } || other.matrix.any { it.size != other.size })
            throw IndexOutOfBoundsException("Matrices line size is inconsistent!")

        DoubleMatrix(size) { lineIndex: Int ->
            DoubleVector(other[0].size) {
                this[lineIndex] dot DoubleVector(other.size) { other[it][lineIndex] }
            }
        }
    }

    operator fun times(other: IntVector) = matrix.run {
        if (this.any { it.size != other.size })
            throw IndexOutOfBoundsException("Matrices are not the right size!")

        IntVector(size) { index ->
            this[index] dot other
        }
    }

    operator fun times(other: LongVector) = matrix.run {
        if (this.any { it.size != other.size })
            throw IndexOutOfBoundsException("Matrices are not the right size!")

        LongVector(size) { index ->
            this[index] dot other
        }
    }

    operator fun times(other: FloatVector) = matrix.run {
        if (this.any { it.size != other.size })
            throw IndexOutOfBoundsException("Matrices are not the right size!")

        DoubleVector(size) { index ->
            this[index] dot other
        }
    }

    operator fun times(other: DoubleVector) = matrix.run {
        if (this.any { it.size != other.size })
            throw IndexOutOfBoundsException("Matrices are not the right size!")

        DoubleVector(size) { index ->
            this[index] dot other
        }
    }
}