package hu.raven.puppet.games.ui.opengl.layer

import hu.raven.puppet.games.logic.game_object.IRenderable
import hu.raven.puppet.games.ui.opengl.Renderer
import hu.raven.puppet.games.utility.math.vector_extension.IntVector

open class GridDependentLayer(
    override val renderer: Renderer,
    override var textureResourceId: Int,
    val size: IntVector
) : ADrawableLayer() {

    override val rectangleCount: Int = size.area
    override lateinit var positionArray: FloatArray
    override lateinit var textureCoordinateArray: FloatArray

    lateinit var dataMatrix: Array<Array<IRenderable>>

    override fun loadVisualBoxesToPositionArray() {
        positionArray = FloatArray(rectangleCount * 6 * renderer.floatPerPositionCoord)

        for (column in 0 until size.x) {
            for (row in 0 until size.y) {
                floatArrayOf(
                    column / 2f, row / 2f + 0.5f,
                    column / 2f, row / 2f,
                    column / 2f + 0.5f, row / 2f + 0.5f,
                    column / 2f, row / 2f,
                    column / 2f + 0.5f, row / 2f,
                    column / 2f + 0.5f, row / 2f + 0.5f,
                ).forEachIndexed { index, coordinate ->
                    positionArray[row * 18 * size.x + column * 18 + index] = coordinate
                }
            }
        }
    }

    override fun loadTextureBoxesToTextureArray() {
        textureCoordinateArray = FloatArray(rectangleCount * 6 * renderer.floatPerTextureCoord)
        //The points texture position initialise
        var rectangleStartIndex = 0
        for (row in dataMatrix.indices) {
            for (cell in dataMatrix[row].indices) {
                dataMatrix[row][cell].let {
                    rotateArray(
                        it.textureBox.serialize(),
                        it.textureBox.rightDirection.toIntVector()
                    ).forEach { trapFloat ->
                        textureCoordinateArray[rectangleStartIndex] = trapFloat
                        rectangleStartIndex++
                    }
                }
            }
        }
    }

    override fun initialize() {
        loadVisualBoxesToPositionArray()
        loadTextureBoxesToTextureArray()
        loadFloatArraysToBuffers()
    }
}