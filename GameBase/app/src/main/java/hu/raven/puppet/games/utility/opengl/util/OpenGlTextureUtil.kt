package hu.raven.puppet.games.utility.opengl.util

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import hu.raven.puppet.games.utility.opengl.facade.OpenGlFacade.generateTextures
import hu.raven.puppet.games.utility.opengl.facade.enumeration.OpenGlPrametriParam
import hu.raven.puppet.games.utility.opengl.facade.enumeration.OpenGlTargetTextureType
import hu.raven.puppet.games.utility.opengl.facade.enumeration.OpenGlTextureParametriValue
import hu.raven.puppet.games.utility.opengl.facade.handle.OpenGlBitmapHandle
import hu.raven.puppet.games.utility.opengl.facade.handle.OpenGlTextureHandle

object OpenGlTextureUtil {
    private fun generateTexture(): OpenGlTextureHandle {
        val handleArray = intArrayOf(0)

        generateTextures(1, handleArray)

        if (handleArray[0] < 0) {
            throw RuntimeException("Error generating texture name.")
        }

        return OpenGlTextureHandle(handleArray[0])
    }

    private fun Context.loadBitmap(resourceId: Int): Bitmap {
        val options = BitmapFactory.Options()

        options.inScaled = false // No pre-scaling

        // Read in the resource
        return BitmapFactory.decodeResource(this.resources, resourceId, options)

    }

    private fun Bitmap.use(function: OpenGlBitmapHandle.() -> Unit) {
        OpenGlBitmapHandle(this).function()
        recycle()
    }

    private fun OpenGlTextureHandle.setMinFilter(parametriValue: OpenGlTextureParametriValue) {
        setTextureParametri(
            OpenGlTargetTextureType.TEXTURE_2D,
            OpenGlPrametriParam.TEXTURE_MAG_FILTER,
            parametriValue
        )
    }

    private fun OpenGlTextureHandle.setMagFilter(parametriValue: OpenGlTextureParametriValue) {
        setTextureParametri(
            OpenGlTargetTextureType.TEXTURE_2D,
            OpenGlPrametriParam.TEXTURE_MIN_FILTER,
            parametriValue
        )
    }


    fun loadOpenGlTexture(context: Context, resourceId: Int): OpenGlTextureHandle =
        generateTexture().apply {

            bindTexture(OpenGlTargetTextureType.TEXTURE_2D)

            setMinFilter(OpenGlTextureParametriValue.CHOOSE_NEAREST_PIXEL)
            setMagFilter(OpenGlTextureParametriValue.CHOOSE_NEAREST_PIXEL)

            context
                .loadBitmap(resourceId)
                .use {
                    setAsTexture(OpenGlTargetTextureType.TEXTURE_2D)
                }
        }
}