package hu.raven.puppet.games.utility.opengl.facade.handle

import android.opengl.GLES20
import hu.raven.puppet.games.utility.opengl.facade.enumeration.OpenGlPrametriParam
import hu.raven.puppet.games.utility.opengl.facade.enumeration.OpenGlTargetTextureType
import hu.raven.puppet.games.utility.opengl.facade.enumeration.OpenGlTextureParametriValue

@JvmInline
value class OpenGlTextureHandle(val textureHandle: Int) {
    fun bindTexture(textureBindTarget: OpenGlTargetTextureType) {
        GLES20.glBindTexture(textureBindTarget.code, textureHandle)
    }

    fun setTextureParametri(
        textureBindTarget: OpenGlTargetTextureType,
        paramName: OpenGlPrametriParam,
        paramValue: OpenGlTextureParametriValue
    ) {
        GLES20.glTexParameteri(
            textureBindTarget.code,
            paramName.code,
            paramValue.code
        )
    }
}