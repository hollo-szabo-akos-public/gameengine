package hu.raven.puppet.games.utility.math

data class IntVector2D(
    val x: Int,
    val y: Int
)