package hu.raven.puppet.games.ui

abstract class APresenter<S> {
    private var screen: S? = null
    open fun attachScreen(screen: S) {
        this.screen = screen
    }

    open fun detachScreen() {
        this.screen = null
    }
}