package hu.raven.puppet.games.ui.opengl.layer

import hu.raven.puppet.games.logic.OClock
import hu.raven.puppet.games.model.DBox
import hu.raven.puppet.games.ui.opengl.Renderer
import hu.raven.puppet.games.utility.EDirection2D
import hu.raven.puppet.games.utility.math.FloatMatrix4x4
import hu.raven.puppet.games.utility.math.VectorUtil.vectorOf
import hu.raven.puppet.games.utility.math.vector_extension.IntVector
import hu.raven.puppet.games.utility.opengl.facade.enumeration.OpenGlDrawMode.DRAW_TRIANGLES
import hu.raven.puppet.games.utility.opengl.facade.enumeration.OpenGlTargetTextureType.TEXTURE_2D
import hu.raven.puppet.games.utility.opengl.util.OpenGlTextureUtil.loadOpenGlTexture
import hu.raven.puppet.games.utility.opengl.util.OpenGlUtil.bindFloatVertexAttributeArray
import hu.raven.puppet.games.utility.opengl.util.OpenGlUtil.bindUniformMatrix
import hu.raven.puppet.games.utility.opengl.util.extention.draw
import hu.raven.puppet.games.utility.opengl.util.extention.toFloatBuffer
import java.nio.FloatBuffer

abstract class ADrawableLayer() {

    init {
        OClock.plusAssign { draw() }
    }

    abstract val textureResourceId: Int

    abstract val renderer: Renderer

    abstract val rectangleCount: Int

    abstract var positionArray: FloatArray

    private lateinit var mPositions: FloatBuffer

    abstract var textureCoordinateArray: FloatArray

    private lateinit var mTextureCoordinates: FloatBuffer

    private var mModelMatrix = FloatMatrix4x4()

    private var mMVPMatrix = FloatMatrix4x4()

    fun rotateArray(
        data: FloatArray,
        direction: IntVector,
    ): FloatArray {
        val temp: FloatArray = data.clone()
        when {
            direction.vector.contentEquals(EDirection2D.LEFT.vector.vector) -> {
                return floatArrayOf(
                    temp[8], temp[9],
                    temp[4], temp[5],
                    temp[2], temp[3],
                    temp[4], temp[5],
                    temp[0], temp[1],
                    temp[2], temp[3]
                )
            }

            direction.vector.contentEquals(EDirection2D.UP.vector.vector) -> {
                return floatArrayOf(
                    temp[4], temp[5],
                    temp[0], temp[1],
                    temp[8], temp[9],
                    temp[0], temp[1],
                    temp[2], temp[3],
                    temp[8], temp[9]
                )
            }

            direction.vector.contentEquals(EDirection2D.RIGHT.vector.vector) -> {
                return temp
            }

            direction.vector.contentEquals(EDirection2D.DOWN.vector.vector) -> {
                return floatArrayOf(
                    temp[2], temp[3],
                    temp[8], temp[9],
                    temp[0], temp[1],
                    temp[8], temp[9],
                    temp[4], temp[5],
                    temp[0], temp[1]
                )
            }

        }
        return floatArrayOf()
    }

    abstract fun loadVisualBoxesToPositionArray()

    abstract fun loadTextureBoxesToTextureArray()

    protected fun loadFloatArraysToBuffers() {
        mPositions = positionArray.toFloatBuffer(0)
        mTextureCoordinates = textureCoordinateArray.toFloatBuffer(0)
    }

    abstract fun initialize()

    fun draw() = renderer.mProgramHandle?.apply {
        loadTextureBoxesToTextureArray()
        loadFloatArraysToBuffers()

        loadOpenGlTexture(renderer.context, textureResourceId)
        renderer.mTextureUniformHandle?.apply {
            activateTexture(0)
            bindUniformTexture(TEXTURE_2D)
            addUniformTextureToTextureSampler(0)
        }

        getVertexAttributeArrayHandle("a_Position").apply {
            bindFloatVertexAttributeArray(
                renderer.floatPerPositionCoord,
                mPositions,
            )
        }

        getVertexAttributeArrayHandle("a_TexCoordinate").apply {
            bindFloatVertexAttributeArray(
                renderer.floatPerTextureCoord,
                mTextureCoordinates,
            )
        }

        mModelMatrix.apply {
            setToIdentity()
            translate(vectorOf(0.0f, 0.0f, -1.0f))
        }
        mMVPMatrix.apply {
            setToIdentity()
            multiplyFromRightAndAssign(renderer.mProjectionMatrix)
            multiplyFromRightAndAssign(renderer.mViewMatrix)
            multiplyFromRightAndAssign(mModelMatrix)
        }
        bindUniformMatrix("u_MVPMatrix", mMVPMatrix)

        positionArray.draw(DRAW_TRIANGLES, 0, renderer.floatPerPositionCoord)
    }


    fun DBox.serialize(): FloatArray = floatArrayOf(
        centerPosition.x - size.x / 2, centerPosition.y + size.y / 2, //left up
        centerPosition.x - size.x / 2, centerPosition.y - size.y / 2, //left down
        centerPosition.x + size.x / 2, centerPosition.y + size.y / 2, //right up
        centerPosition.x - size.x / 2, centerPosition.y - size.y / 2, //left down
        centerPosition.x + size.x / 2, centerPosition.y - size.y / 2, //right down
        centerPosition.x + size.x / 2, centerPosition.y + size.y / 2, //right up
    )

}