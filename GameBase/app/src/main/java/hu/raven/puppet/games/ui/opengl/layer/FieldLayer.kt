package hu.raven.puppet.games.ui.opengl.layer

import hu.raven.puppet.games.logic.game_object.IRenderable
import hu.raven.puppet.games.ui.opengl.Renderer
import hu.raven.puppet.games.utility.EDirection2D
import hu.raven.puppet.games.utility.math.vector_extension.IntVector

class FieldLayer(
    render: Renderer,
    textureFile: Int,
    size: IntVector,
) : GridDependentLayer(render, textureFile, size) {

    private val directions = EDirection2D.values().map { it.vector }

    override fun initialize() {
        loadVisualBoxesToPositionArray()

        val neighborhood = BooleanArray(4) { false }
        var rectangleStartIndex = 0
        for (row in dataMatrix.indices) {
            for (column in dataMatrix[row].indices) {

                for (index in 0 until 4) {
                    neighborhood[index] = column + directions[index].x in dataMatrix[row].indices
                            && row + directions[index].y in dataMatrix.indices
                            && dataMatrix[row + directions[index].y][column + directions[index].x] == dataMatrix[row][column]
                }

                typeToTexture(
                    dataMatrix[row][column],
                    neighborhood
                ).forEach {
                    textureCoordinateArray[rectangleStartIndex] = it
                    rectangleStartIndex++
                }
            }
        }

        loadFloatArraysToBuffers()
    }

    private fun typeToTexture(type: IRenderable, neighborhood: BooleanArray): FloatArray {
        val directionNames = arrayOf(
            "LURD",
            "XURD",
            "LXRD",
            "LUXD",
            "LURX",
            "XXRD",
            "XUXD",
            "XURX",
            "LXXD",
            "LXRX",
            "LUXX",
            "LXXX",
            "XUXX",
            "XXRX",
            "XXXD",
            "XXXX"
        )

        val directionIndex = directionNames.indexOfFirst {
            (0 until 4)
                .all { index ->
                    (neighborhood[index]) xor (it[index] == 'X')
                }
        }

        if (directionIndex == -1) throw Exception("Nem jó a textura meghatározás")

        return type.textureBox.serialize()
    }

}