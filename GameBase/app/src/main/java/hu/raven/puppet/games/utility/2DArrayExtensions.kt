package hu.raven.puppet.games.utility

import hu.raven.puppet.games.utility.math.vector_extension.IntVector

operator fun <T> Array<Array<T>>.get(indexes: IntVector) = this[indexes.y][indexes.x]

inline fun <reified T> Array<Array<T>>.flattened() = this.reduce { sum, current ->
    Array(sum.size + current.size) { index ->
        if (index < sum.size)
            sum[index]
        else
            current[index - sum.size]
    }
}