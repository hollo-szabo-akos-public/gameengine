package hu.raven.puppet.games.utility.math.vector_extension

import hu.raven.puppet.games.utility.math.matrix_extension.DoubleMatrix
import hu.raven.puppet.games.utility.math.matrix_extension.FloatMatrix
import hu.raven.puppet.games.utility.math.matrix_extension.IntMatrix
import hu.raven.puppet.games.utility.math.matrix_extension.LongMatrix
import kotlin.math.sqrt


@JvmInline
value class LongVector(val vector: LongArray) {

    constructor(size: Int, initializer: (it: Int) -> Long) : this(LongArray(size, initializer))

    companion object {
        fun LongArray.toLongVector() = LongVector(this)
        fun unit(size: Int, index: Int) =
            LongVector(size) {
                if (it == index) 1
                else 0
            }
    }

    fun length() = sqrt((this dot this).toDouble())

    val size
        get() = vector.size

    operator fun get(index: Int) = vector[index]

    var x
        get() = vector[0]
        set(value) {
            vector[0] = value
        }

    var y
        get() = vector[1]
        set(value) {
            vector[1] = value
        }

    var z
        get() = vector[2]
        set(value) {
            vector[2] = value
        }

    var w
        get() = vector[3]
        set(value) {
            vector[3] = value
        }

    fun area() = vector.reduce { left, right -> left * right }

    fun toIntVector() = IntVector(size) { this[it].toInt() }
    fun toLongVector() = LongVector(size) { this[it] }
    fun toFloatVector() = FloatVector(size) { this[it].toFloat() }
    fun toDoubleVector() = DoubleVector(size) { this[it].toDouble() }

    operator fun div(other: Int) = vector.run {
        LongVector(size) { index -> get(index) / other }
    }

    operator fun div(other: Long) = vector.run {
        LongVector(size) { index -> get(index) / other }
    }

    operator fun div(other: Float) = vector.run {
        FloatVector(size) { index -> get(index) / other }
    }

    operator fun div(other: Double) = vector.run {
        DoubleVector(size) { index -> get(index) / other }
    }

    infix fun dot(other: IntVector) = vector.run {
        if (other.size != size)
            throw IndexOutOfBoundsException("other array is not the same size!")

        (0 until size).sumOf { index -> this[index] * other[index] }
    }


    infix fun dot(other: LongVector) = vector.run {
        if (other.size != size)
            throw IndexOutOfBoundsException("other array is not the same size!")

        (0 until size).sumOf { index -> this[index] * other[index] }
    }

    infix fun dot(other: FloatVector) = vector.run {
        if (other.size != size)
            throw IndexOutOfBoundsException("other array is not the same size!")

        (0 until size).sumOf { index -> this[index] * other[index].toDouble() }
    }

    infix fun dot(other: DoubleVector) = vector.run {
        if (other.size != size)
            throw IndexOutOfBoundsException("other array is not the same size!")

        (0 until size).sumOf { index -> this[index] * other[index] }
    }

    infix fun match(other: IntVector) = vector.run {
        other.size != size &&
                (0 until size).all { index -> this[index] == other[index].toLong() }
    }

    infix fun match(other: LongVector) = vector.run {
        other.size != size &&
                (0 until size).all { index -> this[index] == other[index] }
    }

    infix fun match(other: FloatVector) = vector.run {
        other.size != size &&
                (0 until size).all { index -> this[index].toFloat() == other[index] }
    }

    infix fun match(other: DoubleVector) = vector.run {
        other.size != size &&
                (0 until size).all { index -> this[index].toDouble() == other[index] }
    }

    operator fun minus(other: IntVector) = vector.run {
        if (other.size != size)
            throw IndexOutOfBoundsException("other array is not the same size!")

        LongVector(size) { index -> get(index) - other[index] }
    }

    operator fun minus(other: LongVector) = vector.run {
        if (other.size != size)
            throw IndexOutOfBoundsException("other array is not the same size!")

        LongVector(size) { index -> get(index) - other[index] }
    }


    operator fun minus(other: FloatVector) = vector.run {
        if (other.size != size)
            throw IndexOutOfBoundsException("other array is not the same size!")

        FloatVector(size) { index -> get(index) - other[index] }
    }


    operator fun minus(other: DoubleVector) = vector.run {
        if (other.size != size)
            throw IndexOutOfBoundsException("other array is not the same size!")

        DoubleVector(size) { index -> get(index) - other[index] }
    }

    operator fun plus(other: IntVector) = vector.run {
        if (other.size != size)
            throw IndexOutOfBoundsException("other array is not the same size!")

        LongVector(size) { index -> get(index) + other[index] }
    }

    operator fun plus(other: LongVector) = vector.run {
        if (other.size != size)
            throw IndexOutOfBoundsException("other array is not the same size!")

        LongVector(size) { index -> get(index) + other[index] }
    }

    operator fun plus(other: FloatVector) = vector.run {
        if (other.size != size)
            throw IndexOutOfBoundsException("other array is not the same size!")

        FloatVector(size) { index -> get(index) + other[index] }
    }

    operator fun plus(other: DoubleVector) = vector.run {
        if (other.size != size)
            throw IndexOutOfBoundsException("other array is not the same size!")

        DoubleVector(size) { index -> get(index) + other[index] }
    }

    operator fun times(other: Int) = vector.run {
        LongVector(size) { index -> get(index) * other }
    }

    operator fun times(other: Long) = vector.run {
        LongVector(size) { index -> get(index) * other }
    }

    operator fun times(other: Float) = vector.run {
        FloatVector(size) { index -> get(index) * other }
    }

    operator fun times(other: Double) = vector.run {
        DoubleVector(size) { index -> get(index) * other }
    }

    operator fun times(other: IntVector) = vector.run {
        if (other.size != size)
            throw IndexOutOfBoundsException("other array is not the same size!")

        LongVector(size) { index -> get(index) * other[index] }
    }

    operator fun times(other: LongVector) = vector.run {
        if (other.size != size)
            throw IndexOutOfBoundsException("other array is not the same size!")

        LongVector(size) { index -> get(index) * other[index] }
    }

    operator fun times(other: FloatVector) = vector.run {
        if (other.size != size)
            throw IndexOutOfBoundsException("other array is not the same size!")

        FloatVector(size) { index -> get(index) * other[index] }
    }

    operator fun times(other: DoubleVector) = vector.run {
        if (other.size != size)
            throw IndexOutOfBoundsException("other array is not the same size!")

        DoubleVector(size) { index -> get(index) * other[index] }
    }

    operator fun times(other: IntMatrix): LongVector {
        if (other.matrix.any { it.size != size })
            throw IndexOutOfBoundsException("Matrices are not the right size!")

        return LongVector(other.size) { index ->
            other[index].dot(this)
        }
    }

    operator fun times(other: LongMatrix): LongVector {
        if (other.matrix.any { it.size != size })
            throw IndexOutOfBoundsException("Matrices are not the right size!")

        return LongVector(size) { index ->
            other[index].dot(this)
        }
    }

    operator fun times(other: FloatMatrix): DoubleVector {
        if (other.matrix.any { it.size != size })
            throw IndexOutOfBoundsException("Matrices are not the right size!")

        return DoubleVector(size) { index ->
            other[index].dot(this)
        }
    }

    operator fun times(other: DoubleMatrix): DoubleVector {
        if (other.matrix.any { it.size != size })
            throw IndexOutOfBoundsException("Matrices are not the right size!")

        return DoubleVector(size) { index ->
            other[index].dot(this)
        }
    }
}