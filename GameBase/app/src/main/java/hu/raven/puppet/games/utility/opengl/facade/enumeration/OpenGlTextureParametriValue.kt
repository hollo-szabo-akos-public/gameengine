package hu.raven.puppet.games.utility.opengl.facade.enumeration

import android.opengl.GLES20

enum class OpenGlTextureParametriValue(val code: Int) {
    CHOOSE_NEAREST_PIXEL(GLES20.GL_NEAREST)
}