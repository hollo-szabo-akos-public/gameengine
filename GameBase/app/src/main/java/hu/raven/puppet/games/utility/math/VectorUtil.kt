package hu.raven.puppet.games.utility.math

object VectorUtil {
    fun vectorOf(
        x: Float,
        y: Float,
    ) = FloatVector2D(x, y)

    fun vectorOf(
        x: Float,
        y: Float,
        z: Float,
    ) = FloatVector3D(x, y, z)

    fun vectorOf(
        x: Float,
        y: Float,
        z: Float,
        w: Float,
    ) = FloatVector4D(x, y, z, w)

    fun vectorOf(
        x: Int,
        y: Int,
    ) = IntVector2D(x, y)
}