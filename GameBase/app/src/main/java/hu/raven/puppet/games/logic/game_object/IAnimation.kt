package hu.raven.puppet.games.logic.game_object

import hu.raven.puppet.games.model.DBox
import hu.raven.puppet.games.utility.math.vector_extension.IntVector

interface IAnimation {
    var frames: List<DBox>
    var currentFrameIndex: Int
    val currentFrame: DBox
        get() = frames[currentFrameIndex]
    var boxSize: IntVector
    var textureSize: IntVector
    fun onStart()
    fun onReset()
    fun onProgress(tickCount: Int)
    fun initializeFrames(startingRow: Int, frameCount: Int)
}