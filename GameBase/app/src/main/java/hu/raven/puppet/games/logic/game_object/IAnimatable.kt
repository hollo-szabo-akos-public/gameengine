package hu.raven.puppet.games.logic.game_object

interface IAnimatable : IRenderable {
    var animationSet: Set<IAnimation>
    var currentAnimation: IAnimation
}