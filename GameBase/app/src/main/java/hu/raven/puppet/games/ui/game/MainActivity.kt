package hu.raven.puppet.games.ui.game

import android.annotation.SuppressLint
import android.app.ActivityManager
import android.content.Context
import android.content.pm.ConfigurationInfo
import android.graphics.PixelFormat
import android.os.Build
import android.os.Bundle
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import hu.raven.puppet.games.R
import hu.raven.puppet.games.R.drawable.character_texture
import hu.raven.puppet.games.databinding.ActivityMainBinding
import hu.raven.puppet.games.injector
import hu.raven.puppet.games.logic.OClock
import hu.raven.puppet.games.logic.game_object.character.Hero
import hu.raven.puppet.games.ui.opengl.Renderer
import hu.raven.puppet.games.ui.opengl.layer.GridFreeLayer
import kotlinx.coroutines.ObsoleteCoroutinesApi
import kotlinx.coroutines.runBlocking
import javax.inject.Inject
import kotlin.concurrent.thread

class MainActivity : AppCompatActivity(), IMainScreen {

    @Inject
    lateinit var gamePresenter: MainPresenter
    private lateinit var render: Renderer
    private lateinit var binding: ActivityMainBinding


    @SuppressLint("ClickableViewAccessibility", "UseCompatLoadingForDrawables")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        injector.inject(this)

        render = Renderer(this)

        render.layers = listOf(
            GridFreeLayer(
                render,
                character_texture
            ).apply {
                dataArray = arrayOf(
                    Hero()
                )
            }
        )

        render.initialize()

        binding.swGameView.setEGLConfigChooser(8, 8, 8, 8, 16, 0)
        binding.swGameView.holder.setFormat(PixelFormat.RGBA_8888)
        binding.swGameView.setZOrderOnTop(true)

        // Check if the system supports OpenGL ES 2.0.
        val activityManager: ActivityManager =
            getSystemService(Context.ACTIVITY_SERVICE) as (ActivityManager)
        val configurationInfo: ConfigurationInfo = activityManager.deviceConfigurationInfo

        if (configurationInfo.reqGlEsVersion >= 0x20000) {
            // Request an OpenGL ES 2.0 compatible context.
            binding.swGameView.setEGLContextClientVersion(2)
            // Set the renderer to our demo renderer, defined below.
            binding.swGameView.setRenderer(render)
        } else {
            // This is where you could create an OpenGL ES 1.x compatible
            // renderer if you wanted to support both ES 1 and ES 2.
            return
        }

        binding.swGameView.setZOrderMediaOverlay(true)
        thread {
            runBlocking {
                OClock.activate()
            }
        }


    }

    @ObsoleteCoroutinesApi
    override fun onResume() {
        super.onResume()
        gamePresenter.attachScreen(this)
        binding.swGameView.onResume()
        OClock += (::updateScreen)
    }

    @ObsoleteCoroutinesApi
    override fun onPause() {
        OClock -= (::updateScreen)
        binding.swGameView.onPause()
        gamePresenter.detachScreen()
        super.onPause()
    }

    private fun updateScreen(count: Int) {
        render.update()
    }
}