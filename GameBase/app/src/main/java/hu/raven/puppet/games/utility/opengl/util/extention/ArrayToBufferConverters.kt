package hu.raven.puppet.games.utility.opengl.util.extention

import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.nio.FloatBuffer
import java.nio.ShortBuffer

fun FloatArray.toFloatBuffer(position: Int = 0): FloatBuffer {
    // (# of coordinate values * 4 bytes per float)
    return ByteBuffer.allocateDirect(size * Float.SIZE_BYTES)
        .run {
            // use the device hardware's native byte order
            order(ByteOrder.nativeOrder())
            // create a floating point buffer from the ByteBuffer
            asFloatBuffer().apply {
                // add the coordinates to the FloatBuffer
                put(this@toFloatBuffer)
                // set the buffer to read the first coordinate
                position(position)
            }
        }
}

fun ShortArray.toShortBuffer(position: Int = 0): ShortBuffer {
    // (# of coordinate values * 2 bytes per float)
    return ByteBuffer.allocateDirect(size * Short.SIZE_BYTES)
        .run {
            order(ByteOrder.nativeOrder())
            asShortBuffer().apply {
                put(this@toShortBuffer)
                position(position)
            }
        }
}