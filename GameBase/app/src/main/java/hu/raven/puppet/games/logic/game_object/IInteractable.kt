package hu.raven.puppet.games.logic.game_object

import hu.raven.puppet.games.model.DBox

interface IInteractable {
    var hitBox: DBox
    fun onHit(interactable: IInteractable)
    fun isHit(box: DBox): Boolean
    fun onCheckIfInteract(tickCount: Int)
}