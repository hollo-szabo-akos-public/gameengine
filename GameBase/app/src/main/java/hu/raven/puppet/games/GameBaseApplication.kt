package hu.raven.puppet.games

import android.app.Application
import hu.raven.puppet.games.ui.UIModule

class GameBaseApplication : Application() {
    lateinit var injector: GameBaseApplicationComponent

    override fun onCreate() {
        super.onCreate()
        injector = DaggerGameBaseApplicationComponent.builder().uIModule(UIModule(this)).build()
    }


}