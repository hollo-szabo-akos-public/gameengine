package hu.raven.puppet.games.logic.game_object.character

import hu.raven.puppet.games.logic.game_object.IAnimatable
import hu.raven.puppet.games.logic.game_object.ICommendable
import hu.raven.puppet.games.logic.game_object.IDynamic

interface ICharacter : IAnimatable, ICommendable, IDynamic {
    var life: Int
    var damage: Int
}