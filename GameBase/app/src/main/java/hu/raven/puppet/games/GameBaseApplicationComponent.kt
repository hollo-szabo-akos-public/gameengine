package hu.raven.puppet.games

import dagger.Component
import hu.raven.puppet.games.ui.UIModule
import hu.raven.puppet.games.ui.game.MainActivity
import javax.inject.Singleton

@Singleton
@Component(modules = [UIModule::class])
interface GameBaseApplicationComponent {
    fun inject(mainActivity: MainActivity)
}