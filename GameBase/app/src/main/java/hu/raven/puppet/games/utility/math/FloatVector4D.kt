package hu.raven.puppet.games.utility.math

data class FloatVector4D(
    val x: Float,
    val y: Float,
    val z: Float,
    val w: Float
)
