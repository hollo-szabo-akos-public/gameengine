package hu.raven.puppet.games.utility.opengl.facade.enumeration

import android.opengl.GLES20

enum class OpenGlSetting(val code: Int) {
    REMOVE_BACK_FACE_BY_CULLING(GLES20.GL_CULL_FACE),
    USE_DEPTH_TEST(GLES20.GL_DEPTH_TEST),
    BLEND_COLORS(GLES20.GL_BLEND),
}