package hu.raven.puppet.games.utility.math

data class FloatVector3D(
    val x: Float,
    val y: Float,
    val z: Float
)
