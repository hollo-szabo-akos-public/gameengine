package hu.raven.puppet.games.logic.game_object.character

import hu.raven.puppet.games.R
import hu.raven.puppet.games.logic.OClock
import hu.raven.puppet.games.logic.game_object.IAction
import hu.raven.puppet.games.logic.game_object.IAnimation
import hu.raven.puppet.games.logic.game_object.IInteractable
import hu.raven.puppet.games.model.DBox
import hu.raven.puppet.games.utility.math.vector_extension.FloatVector
import hu.raven.puppet.games.utility.math.vector_extension.FloatVector.Companion.toFloatVector
import hu.raven.puppet.games.utility.math.vector_extension.IntVector
import hu.raven.puppet.games.utility.math.vector_extension.IntVector.Companion.toIntVector
import kotlinx.coroutines.ObsoleteCoroutinesApi

@ObsoleteCoroutinesApi
class Hero : ICharacter {

    init {
        OClock += ::onRender
        OClock += ::onMove
    }

    override var life: Int = 0

    override var damage: Int = 0

    override var textureId: Int = R.drawable.character_texture

    override var animationSet: Set<IAnimation> = setOf(
        object : IAnimation {

            override lateinit var frames: List<DBox>

            override var boxSize: IntVector = intArrayOf(2, 1).toIntVector()

            override var textureSize: IntVector = intArrayOf(16, 11).toIntVector()

            override var currentFrameIndex: Int = 0

            init {
                initializeFrames(
                    1,
                    16
                )
            }

            override fun onStart() {
                currentFrameIndex = 0
            }

            override fun onReset() {
                currentFrameIndex = 0
            }

            override fun onProgress(tickCount: Int) {
                currentFrameIndex = (currentFrameIndex + 1) % frames.size
            }

            private fun calcTextureBox(
                minRow: Int,
                minColumn: Int,
                maxRow: Int,
                maxColumn: Int,
            ): DBox {
                return DBox(
                    centerPosition = floatArrayOf(
                        (minColumn + (maxColumn - minColumn) / 2.0f) / textureSize.x,
                        (minRow + (maxRow - minRow) / 2.0f) / textureSize.y
                    ).toFloatVector(),
                    size = floatArrayOf(
                        (maxColumn - minColumn) / textureSize.x.toFloat(),
                        (maxRow - minRow) / textureSize.y.toFloat()
                    ).toFloatVector(),
                    rightDirection = floatArrayOf(1.0f, 0.0f).toFloatVector()
                )
            }

            override fun initializeFrames(
                startingRow: Int,
                frameCount: Int
            ) {
                frames = List(frameCount) { frameIndex ->
                    val occupiedColumnsPerRow = textureSize.x - (textureSize.x % boxSize.x)
                    val alreadyOccupiedColumns = frameIndex * boxSize.x
                    calcTextureBox(
                        startingRow + alreadyOccupiedColumns / occupiedColumnsPerRow * boxSize.y,
                        alreadyOccupiedColumns % occupiedColumnsPerRow,
                        startingRow + alreadyOccupiedColumns / occupiedColumnsPerRow * boxSize.y + boxSize.y,
                        alreadyOccupiedColumns % occupiedColumnsPerRow + boxSize.x,
                    )

                }
            }

        }
    )

    override var currentAnimation: IAnimation = animationSet.first()

    override var visualBox: DBox = DBox(
        centerPosition = currentAnimation.boxSize / 2.0f,
        size = currentAnimation.boxSize.toFloatVector(),
        rightDirection = floatArrayOf(1.0f, 0.0f).toFloatVector()
    )
        get() {
            field = DBox(
                centerPosition = field.centerPosition,
                size = currentAnimation.boxSize.toFloatVector(),
                rightDirection = field.rightDirection
            )
            return field
        }

    override var textureBox: DBox
        get() = currentAnimation.currentFrame
        set(value) {}

    override var actionSet: Set<IAction> = setOf()

    override var actionBuffer: MutableList<IAction> = mutableListOf()

    override var acceleration: FloatVector = floatArrayOf(0.0f, 0.0f).toFloatVector()

    override var speed: FloatVector = floatArrayOf(1.0f, 0.0f).toFloatVector()

    override var weight: Float = 1.0f

    override var hitBox: DBox = DBox(
        centerPosition = floatArrayOf(0.0f, 0.0f).toFloatVector(),
        size = floatArrayOf(2.0f, 2.0f).toFloatVector(),
        rightDirection = floatArrayOf(1.0f, 0.0f).toFloatVector()
    )

    //step animation
    override fun onRender(tickCount: Int) {
        currentAnimation.onProgress(tickCount)
    }

    //check if action is complete, and tales next action
    override fun onChoice(tickCount: Int) {

    }

    //updates position
    override fun onMove(tickCount: Int) {

    }

    //reacts to other object
    override fun onHit(interactable: IInteractable) {
        TODO("Not yet implemented")
    }

    //Checks if hit
    override fun isHit(box: DBox): Boolean = false

    //Cheks if any interaction should be maid
    override fun onCheckIfInteract(tickCount: Int) {
        TODO("Not yet implemented")
    }
}