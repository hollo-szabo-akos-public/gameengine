package hu.raven.puppet.games.utility.opengl.facade.enumeration

import android.opengl.GLES20

enum class OpenGlVertexAttributeArrayType(val code: Int) {
    OUT_OF_FLOAT(GLES20.GL_FLOAT)
}