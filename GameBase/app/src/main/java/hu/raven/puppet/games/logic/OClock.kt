package hu.raven.puppet.games.logic

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.*
import kotlin.concurrent.scheduleAtFixedRate
import kotlin.time.Duration
import kotlin.time.Duration.Companion.milliseconds
import kotlin.time.DurationUnit
import kotlin.time.ExperimentalTime

object OClock {

    private val subscribers: MutableList<(tickCount: Int) -> Unit> = mutableListOf()
    private var tickCount = 0
    private var timer: Timer? = null
    private var tickCompleted: Boolean = true


    @ExperimentalTime
    var tickTime: Duration = 100.milliseconds

    operator fun plusAssign(callback: (tickCount: Int) -> Unit) {
        CoroutineScope(Dispatchers.Default).launch {
            subscribers += callback
        }
    }

    operator fun minusAssign(callback: (tickCount: Int) -> Unit) {
        CoroutineScope(Dispatchers.Default).launch {
            subscribers -= callback
        }
    }

    @OptIn(ExperimentalTime::class)
    fun activate() = CoroutineScope(Dispatchers.Default).launch {
        if (timer != null)
            return@launch

        timer = Timer()
        timer!!.scheduleAtFixedRate(0L, tickTime.toLong(DurationUnit.MILLISECONDS)) {
            if (!tickCompleted)
                return@scheduleAtFixedRate
            subscribers.forEach { it(tickCount) }
            tickCount++
            tickCompleted = true
        }
    }


    fun requestStop() {
        timer?.cancel()
        timer = null
    }

    fun clear() {
        tickCount = 0
        subscribers.clear()
        timer?.cancel()
        timer = null
    }
}