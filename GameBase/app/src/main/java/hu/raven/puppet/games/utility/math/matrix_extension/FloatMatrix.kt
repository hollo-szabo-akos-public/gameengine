package hu.raven.puppet.games.utility.math.matrix_extension

import hu.raven.puppet.games.utility.math.vector_extension.DoubleVector
import hu.raven.puppet.games.utility.math.vector_extension.FloatVector
import hu.raven.puppet.games.utility.math.vector_extension.IntVector
import hu.raven.puppet.games.utility.math.vector_extension.LongVector

@JvmInline
value class FloatMatrix(val matrix: Array<FloatVector>) {

    constructor(size: Int, initializer: (index: Int) -> FloatVector)
            : this(Array(size, initializer))

    constructor(size: IntVector, initializer: (indexes: IntVector) -> Float)
            : this(
        Array(size.x) { columnIndex ->
            FloatVector(size.y) { lineIndex ->
                initializer(
                    IntVector(
                        intArrayOf(
                            columnIndex, lineIndex
                        )
                    )
                )
            }
        })

    inline val size
        get() = matrix.size

    operator fun get(indexes: IntVector) = matrix[indexes.y][indexes.x]

    operator fun minus(other: IntMatrix) {
        if (matrix.size != other.size ||
            matrix.mapIndexed { index, line -> line.size != other[index].size }
                .reduce { stuff, value -> stuff || value }
        ) throw IndexOutOfBoundsException("Matrices are not the right size!")

        FloatMatrix(matrix.size)
        { lineIndex ->
            matrix[lineIndex] - other[lineIndex]
        }
    }

    operator fun minus(other: LongMatrix) {
        if (matrix.size != other.size ||
            matrix.mapIndexed { index, line -> line.size != other[index].size }
                .reduce { stuff, value -> stuff || value }
        ) throw IndexOutOfBoundsException("Matrices are not the right size!")

        FloatMatrix(matrix.size)
        { lineIndex ->
            matrix[lineIndex] - other[lineIndex]
        }
    }

    operator fun minus(other: FloatMatrix) {
        if (matrix.size != other.size ||
            matrix.mapIndexed { index, line -> line.size != other[index].size }
                .reduce { stuff, value -> stuff || value }
        ) throw IndexOutOfBoundsException("Matrices are not the right size!")

        FloatMatrix(matrix.size)
        { lineIndex ->
            matrix[lineIndex] - other[lineIndex]
        }
    }

    operator fun minus(other: DoubleMatrix) {
        if (matrix.size != other.size ||
            matrix.mapIndexed { index, line -> line.size != other[index].size }
                .reduce { stuff, value -> stuff || value }
        ) throw IndexOutOfBoundsException("Matrices are not the right size!")

        DoubleMatrix(matrix.size)
        { lineIndex ->
            matrix[lineIndex] - other[lineIndex]
        }
    }

    operator fun plus(other: IntMatrix) {
        if (matrix.size != other.size ||
            matrix.mapIndexed { index, line -> line.size != other[index].size }
                .reduce { stuff, value -> stuff || value }
        ) throw IndexOutOfBoundsException("Matrices are not the right size!")

        FloatMatrix(matrix.size)
        { lineIndex ->
            matrix[lineIndex] + other[lineIndex]
        }
    }

    operator fun plus(other: LongMatrix) {
        if (matrix.size != other.size ||
            matrix.mapIndexed { index, line -> line.size != other[index].size }
                .reduce { stuff, value -> stuff || value }
        ) throw IndexOutOfBoundsException("Matrices are not the right size!")

        FloatMatrix(matrix.size) { lineIndex ->
            matrix[lineIndex] + other[lineIndex]
        }
    }

    operator fun plus(other: FloatMatrix) {
        if (matrix.size != other.size ||
            matrix.mapIndexed { index, line -> line.size != other[index].size }
                .reduce { stuff, value -> stuff || value }
        ) throw IndexOutOfBoundsException("Matrices are not the right size!")

        FloatMatrix(matrix.size) { lineIndex ->
            matrix[lineIndex] + other[lineIndex]
        }
    }

    operator fun plus(other: DoubleMatrix) {
        if (matrix.size != other.size ||
            matrix.mapIndexed { index, line -> line.size != other[index].size }
                .reduce { stuff, value -> stuff || value }
        ) throw IndexOutOfBoundsException("Matrices are not the right size!")

        DoubleMatrix(matrix.size) { lineIndex ->
            matrix[lineIndex] + other[lineIndex]
        }
    }

    operator fun times(other: Int) = matrix.run {
        FloatMatrix(size) { lineIndex ->
            this[lineIndex] * other
        }
    }

    operator fun times(other: Long) = matrix.run {
        FloatMatrix(size) { lineIndex -> this[lineIndex] * other }
    }

    operator fun times(other: Float) = matrix.run {
        FloatMatrix(size) { lineIndex -> this[lineIndex] * other }
    }

    operator fun times(other: Double) = matrix.run {
        DoubleMatrix(size) { lineIndex -> this[lineIndex] * other }
    }

    operator fun times(other: IntMatrix) = matrix.run {
        if (this.any { it.size != other.size })
            throw IndexOutOfBoundsException("Matrices are not the right size!")

        if (this.any { it.size != this[0].size } || other.matrix.any { it.size != other.size })
            throw IndexOutOfBoundsException("Matrices line size is inconsistent!")

        DoubleMatrix(size) { lineIndex ->
            DoubleVector(other[0].size) {
                this[lineIndex] dot IntVector(other.size) { other[it][lineIndex] }
            }
        }
    }

    operator fun times(other: LongMatrix) = matrix.run {
        if (this.any { it.size != other.size })
            throw IndexOutOfBoundsException("Matrices are not the right size!")

        if (this.any { it.size != this[0].size } || other.matrix.any { it.size != other.size })
            throw IndexOutOfBoundsException("Matrices line size is inconsistent!")

        DoubleMatrix(size) { lineIndex ->
            DoubleVector(other[0].size) {
                this[lineIndex] dot LongVector(other.size) { other[it][lineIndex] }
            }
        }
    }

    operator fun times(other: FloatMatrix) = matrix.run {
        if (this.any { it.size != other.size })
            throw IndexOutOfBoundsException("Matrices are not the right size!")

        if (this.any { it.size != this[0].size } || other.matrix.any { it.size != other.size })
            throw IndexOutOfBoundsException("Matrices line size is inconsistent!")

        DoubleMatrix(size) { lineIndex ->
            DoubleVector(other[0].size) {
                this[lineIndex] dot FloatVector(other.size) { other[it][lineIndex] }
            }
        }
    }

    operator fun times(other: DoubleMatrix) = matrix.run {
        if (this.any { it.size != other.size })
            throw IndexOutOfBoundsException("Matrices are not the right size!")

        if (this.any { it.size != this[0].size } || other.matrix.any { it.size != other.size })
            throw IndexOutOfBoundsException("Matrices line size is inconsistent!")

        DoubleMatrix(size) { lineIndex ->
            DoubleVector(other[0].size) {
                this[lineIndex] dot DoubleVector(other.size) { other[it][lineIndex] }
            }
        }
    }

    operator fun times(other: IntVector) = matrix.run {
        if (this.any { it.size != other.size })
            throw IndexOutOfBoundsException("Matrices are not the right size!")

        DoubleVector(size) { index -> this[index] dot other }
    }

    operator fun times(other: LongVector) = matrix.run {
        if (this.any { it.size != other.size })
            throw IndexOutOfBoundsException("Matrices are not the right size!")

        DoubleVector(size) { index -> this[index] dot other }
    }

    operator fun times(other: FloatVector) = matrix.run {
        if (this.any { it.size != other.size })
            throw IndexOutOfBoundsException("Matrices are not the right size!")

        DoubleVector(size) { index -> this[index] dot other }
    }

    operator fun times(other: DoubleVector) = matrix.run {
        if (this.any { it.size != other.size })
            throw IndexOutOfBoundsException("Matrices are not the right size!")

        DoubleVector(size) { index -> this[index] dot other }
    }

    operator fun get(index: Int) = matrix[index]
}