package hu.raven.puppet.games.logic.game_object

import hu.raven.puppet.games.model.DBox
import hu.raven.puppet.games.utility.math.vector_extension.FloatVector.Companion.toFloatVector
import hu.raven.puppet.games.utility.math.vector_extension.IntVector

interface IRenderable {
    var visualBox: DBox
    var textureBox: DBox
    var textureId: Int
    fun initTextureBox(textureSize: IntVector, startPosition: IntVector, boxSize: IntVector) {
        val center = startPosition + (boxSize / 2.0f)
        center.x /= textureSize.x
        center.y /= textureSize.y
        textureBox = DBox(
            center,
            floatArrayOf(
                boxSize.x / textureSize.x.toFloat(),
                boxSize.y / textureSize.y.toFloat()
            ).toFloatVector(),
            floatArrayOf(1.0f, 0.0f).toFloatVector()
        )
    }

    fun onRender(tickCount: Int)
}