package hu.raven.puppet.games.logic.game_object

interface ICommendable {
    var actionSet: Set<IAction>
    var actionBuffer: MutableList<IAction>
    fun onChoice(tickCount: Int)
}