package hu.raven.puppet.games.utility.math

import android.opengl.Matrix
import hu.raven.puppet.games.utility.math.VectorUtil.vectorOf

@JvmInline
value class FloatMatrix4x4 private constructor(val content: FloatArray) {

    constructor() : this(FloatArray(16) { 0f })

    fun setToLookAt(
        eye: FloatVector3D,
        center: FloatVector3D,
        up: FloatVector3D,
        rmOffset: Int = 0
    ) {
        Matrix.setLookAtM(
            content,
            rmOffset,
            eye.x, eye.y, eye.z,
            center.x, center.y, center.z,
            up.x, up.y, up.z,
        )
    }

    fun setToFrustum(
        leftRight: FloatVector2D,
        bottomTop: FloatVector2D,
        neaFar: FloatVector2D,
        offset: Int = 0
    ) {
        Matrix.frustumM(
            content, offset,
            leftRight.x, leftRight.y,
            bottomTop.x, bottomTop.y,
            neaFar.x, neaFar.y
        )
    }

    fun setToIdentity(
        offset: Int = 0
    ) {
        Matrix.setIdentityM(content, offset)
    }

    fun multiplyAndStore(left: FloatMatrix4x4, right: FloatMatrix4x4) {
        Matrix.multiplyMM(content, 0, left.content, 0, right.content, 0)
    }

    fun multiplyFromRightAndAssign(right: FloatMatrix4x4) {
        Matrix.multiplyMM(content, 0, content, 0, right.content, 0)
    }

    fun translate(
        translationVector: FloatVector3D,
        offset: Int = 0
    ) {
        Matrix.translateM(
            content,
            offset,
            translationVector.x,
            translationVector.y,
            translationVector.z
        )
    }

    fun scale(
        scaleAmounts: FloatVector3D,
        offset: Int = 0
    ) {
        Matrix.scaleM(content, offset, scaleAmounts.x, scaleAmounts.y, scaleAmounts.z)
    }

    fun rotate(
        rotation: Float,
        axis: FloatVector3D = vectorOf(0f, 0f, 1f),
        offset: Int = 0
    ) {
        Matrix.rotateM(content, offset, rotation, axis.x, axis.y, axis.z)
    }
}
